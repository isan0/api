<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

class UserController extends Controller
{
    public function store(Request $request)
    {
        $user = User::create($request->all());
        return response()->json($user, 200);
    }

    public function update(Request $request)
    {
        $user = User::find($request->id);
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=$request->password;
        $user= $user->save();
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user = $user->delete();
    }
}
