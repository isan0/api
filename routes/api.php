<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


// Listar todos los productos
Route::get('/product',function(){
    return new ProductCollection(Product::all());
});

// Listar producto individualmente
Route::get('/product/{id}',function($id){
    return new ProductResource(Product::findOrFail($id));
});

// Crear un nuevo producto
Route::post('/product',[ProductController::class,'store']);

// Modificar un producto
Route::put('/product',[ProductController::class,'update']);

// Eliminar un producto
Route::delete('/product/{id}',[ProductController::class,'delete']);


// Listar todos los usuarios
Route::get('/user',function(){
    return new UserCollection(User::all());
});

// Crear un nuevo usuario
Route::post('/user',[UserController::class,'store']);

// Modificar un usuario
Route::put('/user',[UserController::class,'update']);

// Eliminar un usuario
Route::delete('/user/{id}',[UserController::class,'delete']);